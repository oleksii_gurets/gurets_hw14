<?php
namespace classes;
/**
 * Интерфейс строителя
 */
interface ContactBuilder
{
    public function name($name);
    public function surname($surname);
    public function email($email);
    public function phone($phone);
    public function address($address);
    public function build();
}
?>