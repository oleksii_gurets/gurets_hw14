<?php
namespace classes;
/**
 * Строитель нового контакта
 */
class Contact implements ContactBuilder          //Класс-строитель имплементирует интерфейс строителя
{
    protected $contact;                          //Создаём поле, которое будет содержать будущий продукт (объект)

    public function __construct() {
        $this->contact = new ContactProduct;     //Указываем, что создаваемый объект принадлежит классу ContactProduct
    }

    public function name($name){
        $this->contact->name = $name;
        return $this;                                /*Метод устанавливает поле объекта и возращает объект, таким образом
                                                     мы можем обращаться следующим методом по цепочке!*/
    }

    public function surname($surname){
        $this->contact->surname = $surname;
        return $this;
    }

    public function email($email){
        $this->contact->email = $email;
        return $this;
    }

    public function phone($phone){
        $this->contact->phone = $phone;
        return $this;
    }

    public function address($address){
        $this->contact->address = $address;
        return $this;
    }

    public function build() {
        return $this->contact;                        //Финальный метод, возращает готовый объект, замыкая цепочку!
    }
}
?>