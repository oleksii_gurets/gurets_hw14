<?php
spl_autoload_register(function ($className) {
    $filePath = __DIR__ . "/" . str_replace("\\", "/", $className) . ".php";
    if(file_exists($filePath)) {
       require_once str_replace("\\", "/", $className) . ".php";
    } else {
       throw new Exception("Невозможно загрузить $className.");
    }
});

use classes\Contact;

$contact = new Contact();
//$contact теперь объект класса-строителя

$newContact = $contact->name('Bob')
->surname('Dylan')
->email('bob@mail.com')
->build();

echo '<pre>';
print_r($newContact);

$anotherNewContact = $contact->phone('999-555-777')
->name("John")
->surname("Doe")
->email("john@email.com")
->address("John str.")
->build();

print_r($anotherNewContact);

$oneMoreContact = $contact
->email('somecontact@mail.com')
->surname('Someman')
->address('Somestreet str.')
->name('Mike')
->phone('099-789-566')
->build();

print_r($oneMoreContact);
?>